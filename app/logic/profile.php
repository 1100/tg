<?php
require_once('app/database.php');

class Profile
{
    protected static $IS_LOGGED = false;
    protected static $IS_ADMIN = false;

    public static function checkLoginPassword($login, $password)
    {
        $sql_users_query = "SELECT
                        `password`
                      FROM
                        `users`
                          WHERE
                            `login` = '" . $login . "'";
        $sql_users_result = mysql_query($sql_users_query, SQLConnection::get());
        $user = mysql_fetch_assoc($sql_users_result);
        $correct_password = $user['password'];

        if (!empty($correct_password) and ($password == $correct_password)) {
            return true;
        } else {
            return false;
        }
    }

    public static function getCurrentLogin() {
        if(self::isLogged()) {
            return $_SESSION['login'];
        } else {
            return false;
        }
    }

    public static function isLoginRegistered($login) {
        $already_exist = mysql_num_rows(
            mysql_query("SELECT *
                     FROM
                         `users`
                     WHERE
                         `login` = '" . $login . "'"
                , SQLConnection::get()));
        if ($already_exist != 0) {
            return true;
        } else {
            return false;
        }
    }

    public static function isLogged()
    {
        if (!self::$IS_LOGGED) {
            if (isset($_SESSION['login']) && isset($_SESSION['password'])) {
                if (self::checkLoginPassword($_SESSION['login'], $_SESSION['password'])) {
                    self::$IS_LOGGED = true;
                    return true;
                }
                return false;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public static function isAdmin() {
        if (!self::$IS_ADMIN) {
            if (!self::isLogged()) {
                return false;
            } else {
                $users_query = "SELECT `is_admin` FROM `users` WHERE `login` = '" . $_SESSION['login'] . "'";
                $sql_result = mysql_query($users_query, SQLConnection::get());
                $user=mysql_fetch_assoc($sql_result);
                if ($user['is_admin'] == 1) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return self::$IS_ADMIN;
        }
    }

    public static function login($login, $password)
    {
        if (self::checkLoginPassword($login, $password)) {
            $_SESSION['login'] = $login;
            $_SESSION['password'] = $password;
            return true;
        } else {
            return false;
        }
    }

    public static function logout()
    {
        session_destroy();
        header('Location: /');
    }


}

?>
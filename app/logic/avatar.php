<?php
Class Avatar
{

    private static $PATH = 'images/avatars/';
    private static $DEFAULT_AVATAR = 'default.png';
    public static $ext = null;

    public static function upload($file)
    {
        if (self::is_correct($file)) {
            $uniq_name = uniqid();
            $original = $uniq_name . self::getExtension($file['name']);
            $original_path = self::$PATH . $original;
            move_uploaded_file($file["tmp_name"], $original_path);
            $thumbail = self::createThumbnail($original_path, 100);
            self::delete($original);
            return $thumbail;
        } else {
            return false;
        }
    }

    public static function delete($filename)
    {
        if ($filename != self::$DEFAULT_AVATAR) {
            unlink(self::$PATH . $filename);
        }
    }

    private function is_correct($image)
    {
        $filename = $image["name"];
        $file_ext = self::getExtension($filename);
        $file_mime = getimagesize($image["tmp_name"])['mime'];
        $allowed_file_types = array('.jpg', '.jpeg', '.png', '.gif');
        $allowed_mimes = array('image/jpeg', 'image/png', 'image/gif');
        if (
            in_array($file_ext, $allowed_file_types)
            && ($image["size"] < 1024 * 2 * 1024) //2Mb
            && in_array($file_mime, $allowed_mimes)
        ) {
            return true;
        } else {
            return false;
        }
    }

    private function createThumbnail($source_image, $size)
    {
        list($width, $height) = getimagesize($source_image);
        if ($width > $height) {
            $x_pos = ($width - $height) / 2;
            $x_pos = ceil($x_pos);
            $y_pos = 0;
            $new_width = $height;
            $new_height = $height;
        } else {
            $x_pos = 0;
            $y_pos = ($height - $width) / 2;
            $y_pos = ceil($y_pos);
            $new_width = $width;
            $new_height = $width;
        }
        $mime = getimagesize($source_image)['mime'];
        $type = substr(strrchr($mime, '/'), 1);
        switch ($type) {
            case 'png':
                $ImageCreateFunc = 'ImageCreateFromPNG';
                $ImageFunc = 'ImagePNG';
                break;
            case 'gif':
                $ImageCreateFunc = 'ImageCreateFromGIF';
                $ImageFunc = 'ImageGIF';
                break;
            default:
                $ImageCreateFunc = 'ImageCreateFromJpeg';
                $ImageFunc = 'ImageJpeg';
        }
        $image = $ImageCreateFunc($source_image);
        $square_image = ImageCreateTrueColor($new_width, $new_height);
        ImageCopy($square_image, $image, 0, 0, $x_pos, $y_pos, $width, $height);
        $resized = imagecreatetruecolor($size, $size);
        imagecopyresampled($resized, $square_image, 0, 0, 0, 0, $size, $size, $new_width, $new_height);
        $uniq_name = uniqid() . self::$ext;
        $ImageFunc($resized, self::$PATH . $uniq_name);
        return $uniq_name;
    }

    private function getExtension($filename) {
        if (self::$ext == null) {
            self::$ext = substr($filename, strripos($filename, '.'));
        }
        return self::$ext;
    }
}
?>
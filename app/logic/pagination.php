<?php

class Pagination {

    private $BUTTONS_COUNT = 5;

    private $elements_count = null;
    private $elements_per_page = null;
    private $page_count = null;
    private $start_page = null;
    private $end_page = null;
    private $current_page = 1;

    function __construct($elements_count, $elements_per_page, $current_page = null) {
        if (!$elements_count or
            !$elements_per_page) {
            throw new Exception("No data");
        } else {
            if (isset($current_page)){
                if ($current_page < 1) {
                    throw new Exception("Invalid page");
                } else {
                    $this->current_page = $current_page;
                }
            }
            $this->elements_count = $elements_count;
            $this->elements_per_page = $elements_per_page;
            $this->calculatePageCount();
            $this->calculateOutsidePages();
        }
    }

    public function getCurrentPage() {
        return $this->current_page;
    }

    public function getPagesCount() {
        return $this->page_count;
    }

    public function getData() {
        return Array(
            'current_page' => $this->current_page,
            'page_count'   => $this->page_count,
            'start_page'   => $this->start_page,
            'end_page'     => $this->end_page);
    }

    private function calculatePageCount() {
        $this->page_count = (int)($this->elements_count / $this->elements_per_page);
        if ($this->elements_count % $this->elements_per_page) {
            $this->page_count++;
        }
        if ($this->current_page > $this->page_count) {
            throw new Exception("Invalid page");
        }
    }

    private function calculateOutsidePages() {
        $this->calculateStartPage();
        $this->calculateEndPage();
    }

    private function calculateStartPage() {
        if ($this->pagesBeforeCurrent() <= 0) {
            $this->start_page = 1;
        } else {
            $this->start_page = $this->pagesBeforeCurrent();
        }
    }

    private function pagesBeforeCurrent() {
        return $this->current_page - (int)($this->BUTTONS_COUNT/2);
    }

    private function calculateEndPage() {
        if ($this->pagesAfterCurrent() > $this->page_count) {
            $this->end_page = $this->page_count;
        } else {
            $this->end_page = $this->pagesAfterCurrent();
        }
    }

    private function pagesAfterCurrent() {
        return $this->current_page + (int)($this->BUTTONS_COUNT/2);
    }

}
<?php

define('VIEWS_BASEDIR', 'app/views/templates/');

class View
{

    private $views = null;
    protected $data = null;
    private $html = '';
    protected $template = null;
    protected $page_name = null;

    public function __construct($template, $data = null) {
        $this->template = VIEWS_BASEDIR.$template.'.php';
        $this->data = $data;
    }

    private function fetchPartial($content){
        $page_name = null;
        if (isset($this->page_name)) {
            $page_name = $this->page_name;
        }
        if (!empty($this->data) and is_array($this->data)) {
            extract($this->data);
        }
        if (!empty($content) and is_array($content)) {
            extract($content);
        }
        ob_start();
        include $this->template;
        return ob_get_clean();
    }

    public function render(){
        $this->fetchIerarchy();
        echo $this->html;
    }


    private function fetchIerarchy() {
        $content = null;
        if (!empty($this->views)) {
            foreach ($this->views as $key => $all_items) {
                $content[$key] = '';
                foreach ($all_items as $item) {
                    $content[$key] .= $item->fetchIerarchy();
                }
            }
        }
        $this->html =  $this->fetchPartial($content);
        return $this->html;
    }

    public function addView(View $view, $key) {
        if (isset($key)) {
            $this->views[$key][] = $view;
            return true;
        } else {
            return false;
        }
    }

    public function getChild($index = null, $key = null) {
        $view_object = null;
        if (isset($key) and isset($index)) {
            $view_object =  $this->views[$key][$index];
        } else if (isset($index)) {
            $view_object =  $this->views[$index];
        }
        if (is_array($view_object)) {
            return $view_object[0];
        } else {
            return $view_object;
        }
    }
}

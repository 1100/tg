<?php
include 'routing_class.php';
class Route
{

    static function start()
    {
        $router = new routing('app/core/routing.xml');
        $result = $router->get($_SERVER['REQUEST_URI']);

        $controller_file = $result['controller'] . '.php';
        $controller_name = 'Controller_' . $result['controller'];
        $action_name = 'action_' . $result['action'];
        $parameters = $result['values'];

        if (file_exists("app/controllers/".$controller_file)) {
            include "app/controllers/" . $controller_file;
        } else {
            Route::ErrorPage404();
        }

        $controller = new $controller_name;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            $controller->$action($parameters);
        } else {
            Route::ErrorPage404();
        }
    }

    function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }

}
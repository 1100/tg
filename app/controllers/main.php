<?php

require_once('app/views/mainPage.php');

class Controller_Main extends Controller
{

    function __construct() {
    }

    function action_index() {
        $this->view = new MainPageView("Telegram community");
        $this->view->render();
    }

    function action_items() {
        $this->view = new ItemsView($_POST);
        $this->view->render();
    }

    function action_add_item() {
        require_once('app/models/items.php');
        require_once('app/views/ItemView.php');
        $model = new Model_Items();
        if ($model->add($_POST)) {
            $data = $model->getItem();
            $view = new ItemView($data);
            $view->render();
        }
    }

    function action_edit_item() {
        require_once('app/models/items.php');
        require_once('app/views/ItemFormView.php');
        $model = new Model_Items();
        $data = $model->getItem($_POST['id']);
        $view = new ItemFormView($data);
        $view->render();
    }

    function action_cancel_update_item() {
        require_once('app/models/items.php');
        require_once('app/views/ItemView.php');
        $model = new Model_Items();
        $data = $model->getItem($_POST['id']);
        $view = new ItemView($data);
        $view->render();
    }

    function action_update_item() {
        require_once('app/models/items.php');
        require_once('app/views/ItemView.php');
        $model = new Model_Items();
        $model->update($_POST);
        $data = $model->getItem($_POST['id']);
        $view = new ItemView($data);
        $view->render();
    }

    function action_delete_item() {
        require_once('app/models/items.php');
        $model = new Model_Items();
        if($model->delete($_POST['id']))
            echo "Удалено";
        else
            echo "Произошла ошибка";
    }

    function action_login() {
        require_once('app/logic/profile.php');
        if ( !empty($_POST['login']) and !empty($_POST['login']) ) {
            Profile::login($_POST['login'], $_POST['password']);
        }
        header('Location: /');
    }

    function action_logout() {
        require_once('app/logic/profile.php');
        Profile::logout();
        header('Location: /');
    }
}
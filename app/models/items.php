<?php
require_once('app/database.php');
require_once('app/core/model.php');
require_once('app/logic/avatar.php');
class Model_Items extends Model
{
    private $sql_connection;

    function __construct() {
        $this->sql_connection = SQLConnection::get();
    }

    public function add($data) {
        if (!empty($_FILES['avatar']['tmp_name'])) {
            $avatar = Avatar::upload($_FILES['avatar']);
        }
        $sql_query = "
        INSERT INTO
        `items` (
          `items`.`name`,
          `items`.`link`,";
        if (!empty($_FILES['avatar']['tmp_name'])) {
            $sql_query .= "
           `items`.`avatar`,";
        }
          $sql_query .= "
          `items`.`category_id`,
          `items`.`subscribers_count`,
          `items`.`language_id`,
          `items`.`type_id`,
          `items`.`description`
        )
        VALUES (
            '" . $data['name'] . "',
            '" . $data['link'] . "',";
        if (!empty($_FILES['avatar']['tmp_name'])) {
            $sql_query .=  "'" . $avatar . "',";
        }
         $sql_query .= "
            '" . $data['category_id'] . "',
            '" . $data['subscribers_count'] . "',
            '" . $data['language_id'] . "',
            '" . $data['type_id'] . "',
            '" . $data['description'] . "'
        )
        ";
        if(mysql_query($sql_query, $this->sql_connection)) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id) {
        Avatar::delete($this->getAvatar($id));
        $sql_query = "DELETE FROM `items` WHERE `id` = '".$id."'";
        if (mysql_query($sql_query, $this->sql_connection)) {
            return true;
        } else {
            return false;
        }
    }

    public function update($data) {
        $avatar = null;
        if (!empty($_FILES['avatar']['tmp_name'])) {
            Avatar::delete($this->getAvatar($data['id']));
            $avatar = Avatar::upload($_FILES['avatar']);
        }
        $sql_query = "
        UPDATE
        `items`
        SET
            `items`.`name` = '" . $data['name'] . "',
            `items`.`link` = '" . $data['link'] . "',
            `items`.`category_id` = '" . $data['category_id'] . "',
            `items`.`subscribers_count` = '" . $data['subscribers_count'] . "',
            `items`.`language_id` = '" . $data['language_id'] . "',
            `items`.`description` = '" . $data['description'] . "'";
        if (!empty($_FILES['avatar']['tmp_name'])) {
            $sql_query .= "
            , `items`.`avatar` = '" . $avatar ."'";
        }
         $sql_query .= "
        WHERE
            `items`.`id`='" . $data['id'] . "'
        ";
        mysql_query($sql_query, $this->sql_connection);
    }

    public function getTypes() {
        return $this->getIdName('types');
    }

    public function getCategories() {
        return $this->getIdName('categories');
    }

    public function getLanguages() {
        return $this->getIdName('languages');
    }

    private function getIdName($table_name) {
        $sql_query = "
        SELECT
          *
        FROM
          `".$table_name."`
        WHERE
          '1'
        ORDER BY
          id";
        $sql_result = mysql_query($sql_query, $this->sql_connection);
        if ($sql_result == false) {
            return false;
        }
        $result = Array();
        $index = 0;
        while($row = mysql_fetch_array($sql_result)) {
            $result[$index]['id'] = $row['id'];
            $result[$index]['name'] = $row['name'];
            $index++;
        }
        return $result;
    }

    public function getItem($id = null) {
        $sql_query = "
          SELECT
            `items`.`id`,
            `items`.`name`,
            `items`.`link`,
            `items`.`avatar`,
            `items`.`category_id`,
            `items`.`subscribers_count`,
            `items`.`language_id`,
            `items`.`description`,
            `categories`.`name` AS category,
            `languages`.`name` AS lang
          FROM
            `items`
            LEFT JOIN
              `categories`
              ON
                `items`.`category_id` = `categories`.`id`
            LEFT JOIN
              `languages`
              ON
                `items`.`language_id` = `languages`.`id`
          WHERE ";
        if (isset($id)) {
            $sql_query .= "`items`.`id` = '" . $id . "' ";
        } else {
            $sql_query .= "`items`.`id` = (SELECT MAX(`items`.`id`) FROM `items`)";
        }
        $sql_result = mysql_query($sql_query, $this->sql_connection);
        $result = mysql_fetch_assoc($sql_result);
        if ($sql_result) {
            return $result;
        } else {
            return false;
        }
    }

    public function get($options = Array(), $count = 5) {
//        $options['query'];
//        $options['type_id']; - обязательный
//        $options['category_id'];
//        $options['subscribers_count'];
//        $options['language_id'];
        $sql_query = "
          SELECT
            `items`.`id`,
            `items`.`name`,
            `items`.`link`,
            `items`.`avatar`,
            `categories`.`name` AS category,
            `items`.`subscribers_count`,
            `languages`.`name` AS lang,
            `items`.`description`
          FROM
            `items`
            LEFT JOIN
              `categories`
              ON
                `items`.`category_id` = `categories`.`id`
            LEFT JOIN
              `languages`
              ON
                `items`.`language_id` = `languages`.`id`
          WHERE
            `items`.`type_id` = '" . $options['type_id'] . "' ";
        if (!empty($options['query'])) {
            $sql_query .= "
           AND ((
             `items`.`name`
              LIKE '%" . $options['query'] . "%'
              COLLATE utf8_general_ci)
              OR (
              `items`.`link`
              LIKE '%" . $options['query'] . "%'
              COLLATE utf8_general_ci))";
        }
        if (!empty($options['category_id'])) {
            $sql_query .= "AND `items`.`category_id` = '" . $options['category_id'] . "'";
        }
        if (!empty($options['subscribers_count'])) {
            $sql_query .= "AND `items`.`subscribers_count` >= '" . $options['subscribers_count'] . "'";
        }
        if (!empty($options['language_id'])) {
            $sql_query .= "AND `items`.`language_id` = '" . $options['language_id'] . "'";
        }
        $sql_query .= "
          ORDER BY `items`.`id` DESC
          LIMIT ".$options['offset'].",".$count;

        $sql_search_result = mysql_query($sql_query, $this->sql_connection);
        $index = 0;
        $result = Array();
        while ($row = mysql_fetch_assoc($sql_search_result)) {
            $result[$index]['id'] = $row['id'];
            $result[$index]['name'] = $row['name'];
            $result[$index]['link'] = $row['link'];
            $result[$index]['avatar'] = $row['avatar'];
            $result[$index]['category'] = $row['category'];
            $result[$index]['subscribers_count'] = $row['subscribers_count'];
            $result[$index]['lang'] = $row['lang'];
            $result[$index]['description'] = $row['description'];
            $index++;
        }
        if ($index == 0) {
            return false;
        } else {
            return $result;
        }
    }

    public function getCount($options = Array()) {
        $sql_query = "
          SELECT
            COUNT(*) as res
          FROM
            `items`
          WHERE
            `items`.`type_id` = '" . $options['type_id'] . "' ";
        if (!empty($options['query'])) {
            $sql_query .= "
           AND ((
             `items`.`name`
              LIKE '%" . $options['query'] . "%'
              COLLATE utf8_general_ci)
              OR (
              `items`.`link`
              LIKE '%" . $options['query'] . "%'
              COLLATE utf8_general_ci))";
        }
        if (!empty($options['category_id'])) {
            $sql_query .= "AND `items`.`category_id` = '" . $options['category_id'] . "'";
        }
        if (!empty($options['subscribers_count'])) {
            $sql_query .= "AND `items`.`subscribers_count` >= '" . $options['subscribers_count'] . "'";
        }
        if (!empty($options['language_id'])) {
            $sql_query .= "AND `items`.`language_id` = '" . $options['language_id'] . "'";
        }

        $sql_count_result = mysql_query($sql_query, $this->sql_connection);
        $count=mysql_fetch_assoc($sql_count_result);
        return $count['res'];
    }

    private function getAvatar($id) {
        $sql_avatar_query = "
          SELECT
            `items`.`avatar`
          FROM
            `items`
          WHERE
            `items`.`id` = '" . $id . "' ";
        $sql_avatar_result = mysql_query($sql_avatar_query, SQLConnection::get());
        $item = mysql_fetch_assoc($sql_avatar_result);
        return $item['avatar'];
    }

}
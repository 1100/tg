<html>
<head>
    <title><?=$page_name?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
</head>
<body>
    <?=$header?>
    <?=$wrapper?>

</body>
</html>
<form id="item_<?= (isset($id)) ? $id : "add" ?>"
      name="<?= (isset($id)) ? "update_item_" . $id : "add_item" ?>"
      action="<?= (isset($id)) ? "update_item" : "add_item" ?>"
      class="row_form <?= (isset($id)) ? "" : "add_item" ?>">
    <?= (isset($id)) ? '<input type="hidden" name="id" value="' . $id . '">' : "" ?>
    <div class="row <?= (isset($id)) ? "" : "add_item" ?>">
        <div class="name_block wide_col">
            <div class="upload avatar">
                <input type="file" name="avatar"/>
            </div>
            <div>
                <input type="text" name="name" placeholder="Название" value="<?= (isset($name)) ? $name : "" ?>">
                <input type="text" name="link" placeholder="Ссылка" value="<?= (isset($link)) ? $link : "" ?>">
            </div>
        </div>
        <div class="cell">
            Категория
            <br>
            <select name="category_id">
                <?= $category_options ?>
            </select>
        </div>
        <div class="cell">
            Аудитория
            <br>
            <input type="text" name="subscribers_count" class="number"
                   value="<?= (isset($subscribers_count)) ? $subscribers_count : "" ?>">
        </div>
        <div class="cell">
            Язык
            <br>
            <select name="language_id">
                <?= $language_options ?>
            </select>
        </div>
        <div class="cell">
            <div>
                <?= $buttons ?>
            </div>
        </div>
    </div>
    <div class="description_form">
        <textarea name="description" placeholder="Описание"><?= (isset($description)) ? $description : "" ?></textarea>
    </div>
</form>
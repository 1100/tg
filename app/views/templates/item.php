<div class="row" id="item_<?= $id ?>">
    <div class="name_block wide_col tooltip_target">
        <div class="tooltip">
            <img class="avatar" src="/images/avatars/<?= $avatar ?>"><br>
            <a href="https://telegram.me/<?= $link ?>">@<?= $link ?></a><br>
            <?= (isset($description)) ? mb_ereg_replace('\n', '<br />', $description) : "" ?>
        </div>
        <div>
            <img class="avatar" src="/images/avatars/<?= $avatar ?>">
        </div>
        <div class="name_link">
            <div><?= $name ?></div>
            <div><a href="https://telegram.me/<?= $link ?>">@<?= $link ?></a></div>
        </div>
    </div>
    <div class="cell">
        <?= $category ?>
    </div>
    <div class="cell">
        <?= $subscribers_count ?>
    </div>
    <div class="cell">
        <?= $lang ?>
    </div>
    <div class="buttons_block cell">
        <?= $buttons ?>
    </div>
</div>
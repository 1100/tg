<?php
require_once('app/models/items.php');
require_once('app/views/ItemView.php');
require_once('app/views/ItemFormView.php');

class ItemsView extends View
{
    private $model;
    private $options;

    function __construct($options = null) {
        $this->options = $options;
        $this->model = new Model_Items();
        $this->template = 'app/views/templates/items.php';
        if (isset($options)) {
            if (empty($options['type_id'])) {
                $this->options['type_id'] = 1;
            }
            if (empty($options['offset'])) {
                $this->options['offset'] = 0;
            }
        } else {
            $this->options['type_id'] = 1;
            $this->options['offset'] = 0;
        }
        $this->generate();
    }

    private function generate() {
        $data = $this->model->get($this->options);
        if ($data != false) {
            foreach ($data as $item) {
                $this->addView(new ItemView($item), 'item');
                $this->options['offset']++;
            }
        }
        $count = $this->model->getCount($this->options);
        if ($count > $this->options['offset']) {
            $more_items_data = Array('offset' => $this->options['offset'],
                                     'items_left' => $count - $this->options['offset']);
            $more_view = new View('more_items', $more_items_data);
            $this->addView($more_view, 'more');
        }
    }
}
<?php
require_once('app/models/items.php');

class MenuView extends View
{
    private $model;

    function __construct() {
        $this->model = new Model_Items();
        $this->template = 'app/views/templates/menu.php';
        $this->generate();
    }

    private function generate() {
        $data = $this->model->getTypes();
        $is_first = true;
        foreach ($data as $type) {
            $type['selected'] = $is_first;
            $is_first = false;
            $item_view = new View('menu_item', $type);
            $this->addView($item_view, 'menu_item');
        }
    }
}
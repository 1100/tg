<?php
require_once('app/logic/profile.php');

class ItemView extends View
{

    function __construct($data) {
        $this->template = 'app/views/templates/item.php';
        $this->data = $data;
        $this->generate();
    }

    private function generate() {
        $but_data = Array('id' => $this->data['id']);
        if (Profile::isAdmin()) {
            $buttons = new View('item_buttons_admin', $but_data);
            $this->addView($buttons, 'buttons');
        } else {
            $buttons = new View('item_buttons_default', $but_data);
            $this->addView($buttons, 'buttons');
        }
    }
}
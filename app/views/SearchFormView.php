<?php
require_once('app/models/items.php');

class SearchFormView extends View
{
    private $model;

    function __construct() {
        $this->model = new Model_Items();
        $this->template = 'app/views/templates/search_form.php';
        $this->generate();
    }

    private function generate() {
        $category_options = $this->model->getCategories();
        foreach ($category_options as $category_option) {
            $this->addView(new View('option', $category_option), 'category_options');
        }
        $language_options = $this->model->getLanguages();
        foreach ($language_options as $language_option) {
            $this->addView(new View('option', $language_option), 'language_options');
        }
    }
}
<?php
require_once('app/models/items.php');

class ItemFormView extends View
{

    private $model = null;

    function __construct($data = null) {
        $this->template = 'app/views/templates/item_form.php';
        $this->model = new Model_Items();
        $this->data = $data;
        $this->generate();
    }

    private function generate() {
        $category_options = $this->model->getCategories();
        foreach ($category_options as $category_option) {
            if ($this->data['category_id'] == $category_option['id']) {
                $category_option['selected'] = true;
            } else {
                $category_option['selected'] = false;
            }
            $this->addView(new View('option', $category_option), 'category_options');
        }
        $language_options = $this->model->getLanguages();
        foreach ($language_options as $language_option) {
            if ($this->data['language_id'] == $language_option['id']) {
                $language_option['selected'] = true;
            } else {
                $language_option['selected'] = false;
            }
            $this->addView(new View('option', $language_option), 'language_options');
        }
        if (!empty($this->data)) {
            $but_data = Array('id' => $this->data['id']);
            $this->addView(new View('item_buttons_edit', $but_data), 'buttons');
        } else {
            $this->addView(new View('item_buttons_add'), 'buttons');
        }
    }
}
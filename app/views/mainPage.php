<?php
require_once('app/views/HeaderView.php');
require_once('app/views/ItemsView.php');
require_once('app/views/ItemFormView.php');
require_once('app/views/SearchFormView.php');
require_once('app/views/MenuView.php');
require_once('app/logic/profile.php');

class MainPageView extends View
{

    function __construct($title) {
        $this->template = 'app/views/templates/main.php';
        $this->page_name = $title;
        $this->generate();
    }

    private function generate() {
        $this->addView(new HeaderView('header'), 'header');
        $wrapper = new View('wrapper');
        $wrapper->addView(new MenuView(), 'menu');
        $content = new View('content');
        $content->addView(new SearchFormView(), 'search_form');
        $content->addView(new View('table_header'), 'table_header');
        if (Profile::isAdmin()) {
            $content->addView(new ItemFormView(), 'add_item_form');
        }
        $content->addView(new ItemsView(), 'items');
        $wrapper->addView($content, 'content');
        $this->addView($wrapper, 'wrapper');
    }
}
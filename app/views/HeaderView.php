<?php
require_once('app/logic/profile.php');

class HeaderView extends View
{
    function __construct() {
        $this->template = 'app/views/templates/header.php';
        $this->generate();
    }

    private function generate() {
        if (Profile::isLogged()) {
            $data['login'] = Profile::getCurrentLogin();
            $login_view = new View('profile', $data);
        } else {
            $login_view = new View('login_form');
        }
        $this->addView($login_view, 'login');
    }
}
function getCurrentTypeId() {
    return document.getElementsByClassName("current")[0].getAttribute("data-type_id");
}

function getCurrentMenuItem() {
    return document.getElementsByClassName("current")[0];
}

function setMoreItemsEvent() {
    if (document.getElementsByClassName('more')[0]) {
        document.getElementsByClassName('more')[0].onclick = updateItems;
    }
}

function bindEventOnTooltip() {
    var target = document.getElementsByClassName("tooltip_target");
    for (var i = 0; i < target.length; i++) {
        target[i].onmouseover = function () {
            var tooltip = this.getElementsByClassName("tooltip")[0];
            tooltip.style.width = this.offsetWidth - 14 + "px";
            tooltip.style.bottom = (document.body.clientHeight - this.offsetTop + 5) + "px";
        }
    }
}

function bindNumberInputEvent() {
    var number_element = document.getElementsByClassName('number');
    for (var i = 0; i < number_element.length; i++) {
        number_element[i].onkeyup = function () {
            this.value = this.value.replace(/[^0-9]+/, "");
        };
    }
}

function getOffset() {
    if (document.getElementsByClassName('more')[0]) {
        return document.getElementsByClassName('more')[0].getAttribute("data-offset");
    } else {
        return false;
    }
}

function resetSearchForm() {
    document.forms.search_form.reset();
    updateItems();
}

function resetAddForm() {
    document.forms.add_item.reset();
}

function getSearchOptions() {
    var options = new FormData();
    options.append('type_id', getCurrentTypeId());
    options.append('query', document.forms.search_form.query.value);
    options.append('category_id', document.forms.search_form.category_id.value);
    options.append('subscribers_count', document.forms.search_form.subscribers_count.value);
    options.append('language_id', document.forms.search_form.language_id.value);
    return options;
}

function updateItems(event) {
    var xhr = new XMLHttpRequest();
    var options = getSearchOptions();
    if (event && event.target == document.getElementsByClassName('more')[0]) {
        options.append('offset', getOffset());
    }
    xhr.open('POST', '/update_items', true);
    xhr.onload = function() {
            if (event && event.target == document.getElementsByClassName('more')[0]) {
                document.getElementsByClassName('more')[0].remove();
                document.getElementById("items").innerHTML += this.responseText;
            } else {
                document.getElementById("items").innerHTML = this.responseText;
            }
        setMoreItemsEvent();
        bindEventOnTooltip();
    };
    xhr.send(options);
}

window.onload = function(){
    var menu = document.getElementsByClassName("menu")[0];
    var menu_items = document.querySelectorAll(".menu>div");
    var num = menu_items.length-1;
    while(num+1) {
        menu_items[num].onclick = function(event) {
            getCurrentMenuItem().className = "";
            this.className = "current";
            updateItems(event);
        };
        num--;
    }
    document.getElementsByName('query')[0].onkeyup = updateItems;
    document.getElementsByName('category_id')[0].onchange = updateItems;
    bindNumberInputEvent();

    document.forms.search_form.onkeyup = function() {
        updateItems();
    };

    document.getElementsByName('language_id')[0].onchange = updateItems;
    setMoreItemsEvent();

    if (document.forms.add_item) {
        document.forms.add_item.onsubmit = function () {
            var formData = new FormData(document.forms.add_item);
            resetAddForm();
            formData.append('type_id', getCurrentTypeId());
            var xhr = new XMLHttpRequest();
            xhr.open('POST', document.forms.add_item.action, true);
            xhr.onload = function () {
                if (this.response) {
                    var elem = document.createElement('div');
                    elem.innerHTML = this.responseText;
                    elem = elem.firstChild;
                    document.getElementById('items').insertBefore(elem, document.getElementById('items').firstChild);
                    bindEventOnTooltip();
                } else {
                    alert("Ошибка добавления");
                }
            };
            xhr.send(formData);
            return false;
        };
    }

    bindEventOnTooltip();
};

function deleteItem(id) {
    if (confirm("Удалить?")) {
        document.getElementById("item_" + id).remove();
        var xhr = new XMLHttpRequest();
        var options = new FormData();
        options.append('id', id);
        xhr.open('POST', '/delete_item', true);
        xhr.onload = function () {};
        xhr.send(options);
    }
}

function editItem(id) {
    var xhr = new XMLHttpRequest();
    var options = new FormData();
    options.append('id', id);
    xhr.open('POST', '/edit_item', true);
    xhr.onload = function () {
        document.getElementById("item_"+id).outerHTML = this.response;
        bindNumberInputEvent();
    };
    xhr.send(options);
}

function cancelUpdateItem(id) {
    var xhr = new XMLHttpRequest();
    var options = new FormData();
    options.append('id', id);
    xhr.open('POST', '/cancel_update_item', true);
    xhr.onload = function () {
        document.getElementById("item_"+id).outerHTML = this.response;
        bindEventOnTooltip();
    };
    xhr.send(options);
    document.getElementById("item_"+id).onsubmit = function() {return false;};
}

function updateItem(id) {
    var xhr = new XMLHttpRequest();
    var options = new FormData(document.getElementById('item_'+id));
    xhr.open('POST', '/update_item', true);
    xhr.onload = function () {
        document.getElementById("item_"+id).outerHTML = this.response;
        bindEventOnTooltip();
    };
    xhr.send(options);
    document.getElementById("item_"+id).onsubmit = function() {return false;};
}